package diaballik.modele;
public interface Undoable {

	/**
	 *  
	 */
	public void undo();

	/**
	 *  
	 */
	public void redo();

}
