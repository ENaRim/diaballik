# Diaballik

Développement du jeu de société Diaballik.

Frontend : Angular  
Backend : Java  
Api Rest  

# Règle du jeu

Le but du jeu est d'amener sa balle sur la ligne adverse. À son tour, un joueur dispose de trois actions facultatives. Il peut à deux reprises déplacer un pion orthogonalement d'une case (ce peut être le même pion ou deux pions différents) ; il peut une fois transférer la balle d'un pion à un autre, selon le mouvement d'une dame aux échecs. Le transfert ne peut se faire par-dessus un pion adverse.
Ces trois actions, dont aucune n'est obligatoire, peuvent être effectuées dans n'importe quel ordre. Par exemple : déplacement - transfert - déplacement, ou double déplacement - transfert, ou transfert puis simple déplacement, ou déplacement simple ou double sans transfert.
